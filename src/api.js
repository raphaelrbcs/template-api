const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");
const { JsonWebTokenError } = require("jsonwebtoken");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  /* ******************* */
  /* YOUR CODE GOES HERE */
  /* ******************* */

  api.post("/users", async (req, res) => {
    const fields = ['name', 'email', 'password', 'passwordConfirmation'];
    fields.forEach(element => {
      if (!req.body.hasOwnProperty(element)) {
        return res.status(400).send({
          error : "Request body had missing field " + element
        })
      }
    });

    const emailExpr = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    const nameExpr = /^[a-zA-Z0-9]{1,}$/;
    const passwordExpr = /^[a-zA-Z0-9]{8,32}$/;
    if (!emailExpr.test(req.body.email)) {
      return res.status(400).send({
        error : "Request body had malformed field email"
      })
    }
    if (!nameExpr.test(req.body.name)) {
      return res.status(400).send({
        error : "Request body had malformed field name"
      })
    }
    if (!passwordExpr.test(req.body.password)) {
      return res.status(400).send({
        error : "Request body had malformed field password"
      })
    }
    if (req.body.password != req.body.passwordConfirmation) {
      return res.status(422).send({
        error : "Password Confirmation did not match"
      })
    }
    const newUser = {
      id : uuid(),
      name : req.body.name, 
      email : req.body.email,
    };
    const database = mongoClient.db('data');
    const users = database.collection('users');
    const user = await users.findOne({
      name : newUser.name,
      email : newUser.email
    });
    if (user) {
      console.log("User already registered");
    }
    const publishMessage = {
      eventType: "UserCreated",
      entityId: newUser.id, 
      entityAggregate: {
        name : newUser.name,
        email : newUser.email,
        password : newUser.password
      }
    }
    stanConn.publish("users", JSON.stringify(publishMessage), (err, guid) => {
      if (err) {
        console.log(err);
      } else {
        console.log("Published message with guid: " + guid);
      }
    })
    return res.status(201).send({
      user : newUser
    });
  })

  api.delete("/users/:uuid", async (req, res) => {

    let id; 
    try {
      const header = req.headers.authentication;
      const token = header && header.toString().split(" ")[1];
      
      id = jwt.verify(token, secret).id;
      if (id != req.params.uuid) {
        return res.status(403).send({
          error: "Access Token did not match User ID"
        })
      }
    } catch {
      return res.status(401).json({ error: "Access Token not found" });
    }
    const database = mongoClient.db('data');
    const users = database.collection('users');
    const found = await users.findOne({id : id});
    stanConn.publish("users", JSON.stringify(found), (err, guid) => {
      if (err) {
        console.log(err);
      } else {
        console.log("Published message with guid: " + guid);
      }
    })
    return res.status(200).json({id : id});
  });

  return api;
};
